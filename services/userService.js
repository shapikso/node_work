const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getAll() {
        const items = UserRepository.getAll();
        if(!items) {
            throw Error('Users not found');
        }
        return items;
    }

    create(data) {
        const item = UserRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error('User not found');
        }
        return item;
    }

    update(id, data) {
        const item = UserRepository.update(id, data);
        if (!data.firstName && !data.lastName && !data.email && !data.phoneNumber && !data.password) {
            
            throw Error('Nothing to update');
        }
        if(!item) {
            throw Error('User not found');
        }

        if (!item.id) {
            throw Error('Nothing to update');
        } 
        return item;
    }

    delete(id) {
        const item = UserRepository.delete(id);
        if(!item) {
            throw Error('User not found');
        }
        return item;
    }
}

module.exports = new UserService();