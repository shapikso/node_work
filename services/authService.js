const UserService = require('./userService');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        console.log('--------------------------------------------------------------------------------------');
        console.log(user);
        console.log('--------------------------------------------------------------------------------------');
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
}

module.exports = new AuthService();