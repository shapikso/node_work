const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll() {
        const items = FighterRepository.getAll();
        if(!items) {
            throw Error('Fighters not found');
        }
        return items;
    }

    create(data) {
        if(!data.health) {
            data.health = 100;
        }
        const item = FighterRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }

    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            throw Error('Fighter not found');
        }
        return item;
    }

    update(id, data) {
        const item = FighterRepository.update(id, data);
        if (!data.name && !data.health && !data.defense && !data.power) {
            
            throw Error('Nothing to update');
        }else if(!item) {
            throw Error('Fighter not found for update');
        } else if (!item.id) {
            throw Error('Nothing to update');
        }
        return item;
    }

    delete(id) {
        const item = FighterRepository.delete(id);
        if(!item) {
            throw Error('Fighter not found');
        }
        return item;
    }
}

module.exports = new FighterService();