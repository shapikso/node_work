const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        console.log('--------------------------------------------------------------------------------------');
        console.log(req.body);
        console.log(req.route);
        console.log('--------------------------------------------------------------------------------------');
        const data = AuthService.login(req.body);
        res.data = data;
    } catch (err) {
        res.err = {
          error: true,
          message: 'Login or passwor isn`t correct'
        }
    } finally {
        next();
    }
}, responseMiddleware);

module.exports = router;