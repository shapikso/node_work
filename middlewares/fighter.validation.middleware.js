const { fighter } = require('../models/fighter');

const fighterSchema = {
    name: (value) => value !== undefined && value.length > 0,
    power: (value) =>
      value !== undefined && parseInt(value, 10) && value < 100 && value > 0,
    defense: (value) =>
      value !== undefined && parseInt(value, 10) && value <= 10 && value > 0,
    health: (value) =>
      value !== undefined && parseInt(value, 10) && value <= 120 && value > 79,
  }

  

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation

    const data = req.body;
    res.err = createValidate(data, fighterSchema);

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    const data = req.body;
    res.err = updateValidate(data, fighterSchema);

    next();
}

const updateValidate = (object, schema) => {
  const invalid = findInvalidKeys(object, schema)
  let error = {
      error: false,
      message: '',
    };
   
  if (invalid.length > 0) {
      try{   
          throwInvalidKey(invalid[0]);
        } catch(err){ 
          error = err;
        }
  }
  return error;
}

const createValidate = (object, schema) => {
  const invalid = findInvalidKeys(object, schema)
  let error = {
      error: false,
      message: '',
    };
  if (invalid.length > 0 && invalid[0]!=='health') {
      try{   
          throwInvalidKey(invalid[0]);
        } catch(err) { 
          error = err;
        }
  }
  const requiredKeys = findRequiredKeys(object, schema)
  if (requiredKeys.length > 0 && requiredKeys[0]!=='health') {
  try{  
    throwInvalidKey(requiredKeys[0]);
  } catch(err) { 
      error = err;
   }
  }
  return error;
}

const throwInvalidKey = (key) => {
const err = {
  error: true,
  message: `${key} is invalid`,
}
throw err;
}

const findRequiredKeys = (object, schema) => Object.keys(schema).filter((key) => object[key] === undefined)

const findInvalidKeys = (object, schema) => Object.keys(object).filter((key) =>  schema[key] === undefined || !schema[key](object[key]))

  
  
  
  
exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;