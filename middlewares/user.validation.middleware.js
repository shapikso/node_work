const { user } = require('../models/user');
const userSchema = {
    email: (value) =>
      value !== undefined &&
      /([a-zA-Z0-9]+)([\.{1}])?([a-zA-Z0-9]+)\@gmail([\.])com/g.test(value),
    phoneNumber: (value) => value !== undefined && /^\+380\d{9}$/g.test(value),
    password: (value) => value !== undefined && value.length >= 3,
    firstName: (value) => value !== undefined && value.length > 0,
    lastName: (value) => value !== undefined && value.length > 0,
  }
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    const data = req.body;
    const errorIsTrue = createValidate(data, userSchema);
    res.err = errorIsTrue;
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    const data = req.body;
    const errorIsTrue = updateValidate(data, userSchema);
    res.err = errorIsTrue;
    next();
}

const updateValidate = (object, schema) => {
    const invalid = findInvalidKeys(object, schema)
    let error = {
        error: false,
        message: '',
      };
    if (invalid.length > 0) {
        try{   
            throwInvalidKey(invalid[0]);
          } catch(err){ 
            error = err;
          }
    }
    return error;
  }

const createValidate = (object, schema) => {
    const invalid = findInvalidKeys(object, schema)
    let error = {
        error: false,
        message: '',
      };
    if (invalid.length > 0) {
        try{   
            throwInvalidKey(invalid[0]);
          } catch(err) { 
            error = err;
          }
    }
    const requiredKeys = findRequiredKeys(object, schema)
    console.log(requiredKeys);
    if (requiredKeys.length > 0) {
    try{  
      throwInvalidKey(requiredKeys[0]);
    } catch(err) { 
        error = err;
     }
    }
    return error;
  }
  
  const throwInvalidKey = (key) => {
  const err = {
    error: true,
    message: `${key} is invalid`,
  }
  throw err;
}
  
  const findRequiredKeys = (object, schema) => Object.keys(schema).filter((key) => object[key] === undefined)

  const findInvalidKeys = (object, schema) => Object.keys(object).filter((key) =>  schema[key] === undefined || !schema[key](object[key]))

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
