const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
  if(res.err) {
       if (res.err.message.indexOf('not found') !== -1 || res.err.message.indexOf('isn`t correct') !== -1 ){ 
    res.status(404).send(res.err)
  }
   else if (res.err.message.indexOf("is invalid") !== -1 || res.err.message.indexOf('Nothing to') !== -1){
    res.status(400).send(res.err)
  } else if ((!res.err.error)) {
    res.status(200).send(res.data);
    }
}
  else {
    res.status(200).send(res.data);
    }
 next();
}

exports.responseMiddleware = responseMiddleware;

